# A Simple message service in Go

This is a simple message handling application written in GO.

## Features

- store message
- retrieve message in chronological order
- messages are stored in [memory](#memory-storage)
- [http/s server](#server-command)
- [tls](#tls)
- [hot reload configuration](#dynamic-configuration)

## CLI

Usage:

```
Usage:
  cli [command]

Available Commands:
  help        Help about any command
  server      The 'server' command runs the simple message logging server.
```

### `server` Command

`server` subcommand starts http server ([see http rest api](#http-rest-api))

When no additional configuration is provided it listens on all interfaces (e.g. 0.0.0.0) and port 5000

Usage:

```
Usage:
  cli server [flags]

Flags:
  -l, --address ip        The ip address server will listen on. (default 0.0.0.0)
  -h, --help              help for server
  -p, --port uint16       The port number server will listen on. (default 5000)
      --tls-cert string   TLS certificate.
      --tls-key string    TLS key.
```

#### TLS

Application supports a TLS encryption when valid TLS Certificate (`--tls-cert`) and and TLS Private Key (`--tls-key`) is provided.

`certs` directory contains development certifications generated on MacOS using [mkcert utility](https://github.com/FiloSottile/mkcert)

### Configuration

The application can be configured using following inputs; **each item takes precedence over the item below it**:
- cli flags
- environment variables
- configuration file 
- default values

#### CLI configuration

```
$ <executable> -h 
```

```
Usage:
  cli [command]

Available Commands:
  help        Help about any command
  server      The 'server' command runs the simple message logging server.

Flags:
      --config string   config file (default is config.toml) (default "config.toml")
  -h, --help            help for cli

Use "cli [command] --help" for more information about a command.
```

Each command has it own help context e.g. using `server` command:

```
$ <executable> server -h 
```

```
The 'server' command runs the simple message logging server.

Usage:
  cli server [flags]

Flags:
  -l, --address ip        The ip address server will listen on. (default 0.0.0.0)
  -h, --help              help for server
  -p, --port uint16       The port number server will listen on. (default 5000)
      --tls-cert string   TLS certificate.
      --tls-key string    TLS key.

Global Flags:
      --config string   config file (default is config.toml) (default "config.toml")

```

#### ENV

Application supports environment variables:

> `SML` prefix is used for whole application.

`SML_SERVER_ADDRESS=<value>`

`SML_SERVER_PORT=<value>`

##### Examples

- Listening ip address:<br/>
  `SML_SERVER_ADDRESS=127.0.0.1 go run cmd/main.go`

- Port<br/>
  `SML_SERVER_PORT=5010 go run cmd/main.go`

#### Configuratiaon file

Application supports following format of the configuration file:

1. TOML

``` TOML
[server]
port = 8383
address = "127.0.0.1"
```

2. YAML

``` YAML
server:
  port: 8383
  address: 127.0.0.1
```

3. JSON

``` json
{
  "server": {
    "port": 8383,
    "address": "127.0.0.1"
  }
}
```

All parameters in the configuration file are eligible for [dynamic configuration](#dynamic-configuration).

#### Dynamic configuration
Application support configuration change detection (aka: _hot reload_). When application detects changes in the [configuration file](#configuratiaon-file) it tries to apply them.

> Since CLI flags and ENV variable take precedence over values in the configuration file, changing value in the configuration file may not lead to change in the application after the _hot reaload_.

## Testing

Run all **unit tests** in the codebase

``` 
$ go test ./...
```

Run all **integration tests** in the codebase. (e.g. spins up a real HTTP server and uses http rest communication)

Integration tests run also a parallel POST workload with 4 concurrent clients. Meanwhile another client GET last messages.

```
$ go test --tags=integration ./...
```

## API
The application currently implements only REST HTTP/s server using HTTP rest

### HTTP REST API

#### `GET /messages`

Returns a maximum of 100 messages in chronological order (ascending); or an empty array.

##### Request

``` HTML
GET /messages
Content-type: application/json
```

##### Response

``` HTML
Content-type: application/json
```

``` json
[
    {
      "msg": "some message",
      "ts": 123
    },
    {
      "msg": "some other message",
      "ts": 123
    },
    {
      "msg": "another newer message",
      "ts": 124
    }
]
```

JSON schema of the payload

``` json
{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "array",
  "title": "The Root Schema",
  "items": {
    "$id": "#/items",
    "type": "object",
    "title": "The Items Schema",
    "required": [
      "msg",
      "ts"
    ],
    "properties": {
      "msg": {
        "$id": "#/items/properties/msg",
        "type": "string",
        "title": "The message",
        "default": "",
        "examples": [
          "some message"
        ],
        "pattern": "^(.*)$"
      },
      "ts": {
        "$id": "#/items/properties/ts",
        "type": "integer",
        "title": "Timestamp of the message",
        "description": "UNIX Time (UTC; seconds)",
        "default": 0,
        "examples": [
          123
        ]
      }
    }
  }
}
```

#### `POST /message`

Endpoint adds a new message to the storage.

It returns `400 Bad Request` should the payload does not conform to the JSON Schema.

##### Request

``` HTML
POST /message
Content-type: application/json
```

``` json
{
  "msg": "some message",
  "ts": 1566461840
}
```

JSON schema of the payload

``` json
{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "The Root",
  "required": [
    "msg",
    "ts"
  ],
  "properties": {
    "msg": {
      "$id": "#/properties/msg",
      "type": "string",
      "title": "The message",
      "default": "",
      "examples": [
        "some message"
      ],
      "pattern": "^(.*)$"
    },
    "ts": {
      "$id": "#/properties/ts",
      "type": "integer",
      "title": "Timestamp of the message",
      "description": "UNIX Time (UTC; seconds)",
      "default": 0,
      "examples": [
        1566461840
      ]
      }
  }
}
```

##### Response

``` HTML
Content-type: application/json
```

``` json
{
  "status": "ok"
}
```

JSON schema of the payload

``` json
{
  "definitions": {},
  "$schema": "",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "The Root",
  "default": null,
  "required": [
    "status"
  ],
  "properties": {
    "status": {
      "$id": "#/properties/status",
      "type": "string",
      "title": "The Status",
      "default": "",
      "examples": [
        "ok"
      ],
      "pattern": "(^ok$|^nok$)"
    }
  }
}

```

# Code

The code is structured in two main packages: 
- cmd
- pkg

## `cmd`

`cmd` package contains the [main.go](cmd/main.go) and entrypoint to the application. 

CLI and configuration uses [Cobra](https://github.com/spf13/cobra) nad [Viper](https://github.com/spf13/viper) toolset for easy configuration management.

Commands are in `cmd/commands` folder

## `pkg`

`pkg` package contains the main domains of the application. Each package exposes its models and interfaces which allow extensibility and testability with its [trade-off](#interface-performance).

### `core`
- config (contains configuration structures)
+ adding 
+ listing
- storage
+ http

The structure can be grouped to logical sub-packages, but I prefer to keep the structure rather flat.

### Memory Storage

Memory storage is implemented as basic vector (dynamic array) but for testing reasons there is a dequeing memory storage (<a href="pkg/storage/dequeMemory.go">dequeMemory.go</a>) which has a limited capacity specified during construction. When the capacity is filled, the oldest message is discarded when newer message is stored.

# Docker

[Dockerfile](Dockerfile) support uses multistage build using Go 1.13-alpine image

Docker file runs `server` command `cmd`

Exposes default port 5000

Volume `/app/data`

By default running the container would start the HTTP/s server listening on on port 5000 on all interfaces.

# Notes and Todos

## Interface Performance

Using interfaces extensively forces v-tables lookups thus disables inlining in most cases causing perfomance drop; service structs are implemneted as prive for its package which is good for clarity of the code, however _factory_ functions (e.g. `New...`) may be extended and return concrete strucutre shoud they are changed to public, and loosing polymorphysm on the way.

This fact should not be a surprise - most if not all languages _suffer_ from this performance loss when using _**dynamic method dispatch**_. 

## Better sorting

The application currently uses _naìve_ `on-insert` sorting. Ideally, this would be handled byt some sort of time-series database like [InfluxDB](https://www.influxdata.com/), or some kind of memory-mapped file with file custom logic, flusing, sorting and indexing.

## Other Improvements

- Redirect when HTTP to HTTPS when TLS is used or allow both
- Add parameter to CA
- Enforce TLS version
- Add CLI for to manage messages
- ...

# Tools and 3rd party packages

- golang 1.13
- [cobra](https://github.com/spf13/cobra)
- [viper](https://github.com/spf13/viper)
- [gorilla mux](http://www.gorillatoolkit.org/pkg/mux)

# Q&A

## Propose and describe (optionally implement) how would you run the program in production environment

- Add security headers
  - Access-Control-Allow-Methods: GET, POST,
  - Strict-Transport-Security
  - X-Frame-Options: Deny
- Add JWT authentication middleware, if requested.
- Add request limiter middleware ans similar strategies.
- Improve and set limit of the payloads validations (currently "unlimited" payload is allowed by the server; timeout is being set). 
- Allow to configure timeouts.
- Parametrize storage kind - influxdb, server, filesystem, memory .
- The application is missing logging and metrics tools. For production deployment a better, configurable logging, which would allow to change (dynamically) verbosity of logs, target (logging server or at least rolling file output; ideally both). It would be also worth-while implement metrics to logs (e.g. latency).
- Make sure data backup and restore is working.
- Use Ceph as distributed filesystem if filesystem is chosen as the storage option.
- Current implementation *targets heavy read scenarios*. RWLock structure allows multiple parallel read access to the underlying storage. If a heavy write scenario happens there may be a *memory exhaustion*.

  
## 1. How would you handle deploy

Answer very much depends on a given infrastructure and non-functional requirements (NFR) as well as if data migration is needed.

The simpliest solution would be to run as daemon service in Linux environment (or Windows service in Windows environment)...

Example of Unit file in Linux:

```
[Unit]
Description=Simple Message Service
After=network.target

[Service]
ExecStart=/usr/bin/sms_hw --config /etc/livesport/config.conf
User=sms_hw
Group=sms_hw
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
```

### Docker, Traefic, Consul, (and Nomad)

This would be my go to deployment startegy. The trion in the header allows dynamic service discovery, blue/green or canary deployments across multiple servers, data-centers, clusters and therefore allows zero-downtime, as well as deployment on single system. It has integrated metrics and simple logging with options to attach additional infrastructure services such as logstash, zipkin etc.

_Nomad_ would also help with management of the systems and applications.

### Kubernetes, Terraform, Vault, Ceph

There are many ways to do deployment.

Should it be deployed in the cloud, hybrid cloud atc... terraform would be my go to tool. If the system would grow towards microservices architecture, I'd choose K8s with Terraform and Vault with

## 2. How would you apply updates

As stated in question about deploy, it depends on given infrastructure and non-functional requirements (NFR) as well as if data migration is needed.

My go to solution and tools would be Docker, Traefik and Consul or K8s. With help of _Nomad_ as its commander it would hold against probably most of the scenarios. As usual, most complicated scenarios would be with data migration.

The CI pipeline would hopefully filter off most of the mistakes (either build failure, or any integration test fails).

With new docker image build, Nomad would command its nodes to perform a rolling update to the newest version of the Docker image.

Should there was a failure the old image would be automatically deployed back.

Traefik would handle load balancing and routing to stable services.

All this infrastructure would be backed up by Consul.

## 3. How would you monitor state of the service
Ideally I would use ELK with Grafana as the monitoring solution.

## 4. How would you achieve it running 24/7, even if it fails
Properly configured unit file for systemd would make sure the services is always running.

Docker with `--restart unless-stopped` would make sure the container is always running

Similarly with correct configuration every mentioned _**declarative**_ system would try to make sure the service is always running.