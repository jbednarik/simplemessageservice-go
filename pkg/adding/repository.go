package adding

type Repository interface {
	Add(message Message) error
}
