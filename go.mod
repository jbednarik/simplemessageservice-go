module livesport_hw

go 1.13

require (
	github.com/fsnotify/fsnotify v1.4.7
	github.com/gorilla/mux v1.7.3
	github.com/mitchellh/go-homedir v1.1.0
	github.com/mitchellh/mapstructure v1.1.2
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.6.1
)
