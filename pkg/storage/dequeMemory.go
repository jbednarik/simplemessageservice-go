package storage

import (
	"livesport_hw/pkg/adding"
	"livesport_hw/pkg/listing"
	"sync"
)

type DequeMemory struct {
	memory *Memory
	cap    int
	rwLock sync.RWMutex
}

func NewDequeMemory(limit int) *DequeMemory {
	if limit <= 1 {
		panic("limit must be > 1")
	}
	return &DequeMemory{
		memory: NewMemory(),
		cap:    limit,
	}
}

func (d *DequeMemory) GetLastMessages(limit int) []listing.Message {
	d.rwLock.RLock()
	defer d.rwLock.RUnlock()

	offset := GetLastItemsOffset(len(d.memory.data), limit)
	messages := d.memory.data[offset:]
	vec := make([]listing.Message, len(messages), len(messages))
	for i, msg := range messages {
		vec[i] = listing.Message{
			Timestamp: msg.Timestamp,
			Message:   msg.Message,
		}
	}
	return vec
}

func (d *DequeMemory) Add(message adding.Message) error {
	d.rwLock.Lock()
	defer d.rwLock.Unlock()

	if len(d.memory.data) >= d.cap {
		newData := make([]Message, 0, d.cap)
		for _, d := range d.memory.data[1:] {
			newData = append(newData, d)
		}
		d.memory.data = newData
	}
	return d.memory.AddMessage(message)
}
