package rest

import (
	"bytes"
	"encoding/json"
	"livesport_hw/pkg/adding"
	"livesport_hw/pkg/listing"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestAddMessagesHandler(t *testing.T) {
	logger := log.New(os.Stdout, "", log.LstdFlags)

	t.Run("add valid message", func(t *testing.T) {

		addingSvc := &addingSvc{data: make([]adding.Message, 0)}
		services := MessageServices{
			Adding:  addingSvc,
			Listing: nil,
		}

		handlers := newMessageHandlers(logger, services)

		ts := int64(1)
		message := "123"
		reqPayload := PostMessageRequest{
			Timestamp: &ts,
			Message:   &message,
		}
		reqBodyBytes, err := json.Marshal(&reqPayload)
		if err != nil {
			t.Fatal(err)
		}
		req, err := http.NewRequest(http.MethodPost, "/message", bytes.NewBuffer(reqBodyBytes))

		rr := httptest.NewRecorder()
		http.HandlerFunc(handlers.PostMessageHandler).ServeHTTP(rr, req)

		// assert handler called correct service
		{
			if len(addingSvc.data) != 1 {
				t.Errorf("adding service was not called")
			}
		}

		// assert code
		assertCode(t, rr.Code, http.StatusOK)

		// assert response
		{
			var got PostMessageResponse
			dec := json.NewDecoder(rr.Body)
			err = dec.Decode(&got)
			if err != nil {
				t.Fatal(err)
			}

			want := PostMessageResponse{Status: PostMessageStatusOk}

			if got != want {
				t.Errorf("got: %v, want: %v", got, want)
			}
		}
	})

	t.Run("add invalid message (missing field)", func(t *testing.T) {
		addingSvc := &addingSvc{data: make([]adding.Message, 0)}
		services := MessageServices{
			Adding:  addingSvc,
			Listing: nil,
		}

		handlers := newMessageHandlers(logger, services)

		req, err := http.NewRequest(http.MethodPost, "/message", bytes.NewBufferString(`{"ts": 1566461840}`))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		http.HandlerFunc(handlers.PostMessageHandler).ServeHTTP(rr, req)

		// assert handler called correct service
		{
			if len(addingSvc.data) != 0 {
				t.Errorf("adding service was called")
			}
		}

		// assert code
		assertCode(t, rr.Code, http.StatusBadRequest)

		// assert response
		{
			var got PostMessageResponse
			dec := json.NewDecoder(rr.Body)
			err = dec.Decode(&got)
			if err != nil {
				t.Fatal(err)
			}

			want := PostMessageResponse{Status: PostMessageStatusNok}

			if got != want {
				t.Errorf("got: %v, want: %v", got, want)
			}
		}
	})
}

func TestGetMessagesHandler(t *testing.T) {
	logger := log.New(os.Stdout, "", log.LstdFlags)

	listingSvc := &listingSvc{data: make([]listing.Message, 0)}
	for i := 0; i < 100; i++ {
		listingSvc.data = append(listingSvc.data, listing.Message{
			Timestamp: int64(i),
			Message:   "Hello",
		})
	}
	services := MessageServices{
		Adding:  nil,
		Listing: listingSvc,
	}

	handlers := newMessageHandlers(logger, services)

	req, err := http.NewRequest(http.MethodGet, "/messages", nil)

	rr := httptest.NewRecorder()
	http.HandlerFunc(handlers.GetMessagesHandler).ServeHTTP(rr, req)

	// assert code
	assertCode(t, rr.Code, http.StatusOK)

	// assert response
	{
		var got []GetMessageResponse
		dec := json.NewDecoder(rr.Body)
		err = dec.Decode(&got)
		if err != nil {
			t.Fatal(err)
		}

		if len(got) != len(listingSvc.data) {
			t.Fatal("received incorrect number of messages")
		}

		for i, response := range got {
			if response.Timestamp != listingSvc.data[i].Timestamp {
				t.Fatalf("incorrect data: got: %v, want: %v", response, listingSvc.data[i])
			}

			if response.Message != listingSvc.data[i].Message {
				t.Fatalf("incorrect data: got: %v, want: %v", response, listingSvc.data[i])
			}
		}
	}
}

func assertCode(t *testing.T, got int, want int) {
	if got != want {
		t.Errorf("got: %d, want: %d", got, want)
	}
}

type addingSvc struct {
	data []adding.Message
}

func (a *addingSvc) AddMessage(messages ...adding.Message) error {
	a.data = append(a.data, messages...)
	return nil
}

type listingSvc struct {
	data []listing.Message
}

func (l *listingSvc) GetLastMessages(limit int) []listing.Message {
	return l.data
}
