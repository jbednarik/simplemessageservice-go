package listing

type Service interface {
	GetLastMessages(limit int) []Message
}

type service struct {
	rpo Repository
}

func (s *service) GetLastMessages(limit int) []Message {
	return s.rpo.GetLastMessages(limit)
}

func NewService(rpo Repository) *service {
	return &service{rpo: rpo}
}
