package listing

type Message struct {
	Timestamp int64  `json:"ts"`
	Message   string `json:"msg"`
}
