package storage

import (
	"livesport_hw/pkg/adding"
	"livesport_hw/pkg/listing"
	"math"
	"sort"
	"sync"
)

type Message struct {
	Timestamp int64
	Message   string
}

type Memory struct {
	data   []Message
	rwLock sync.RWMutex
}

func NewMemory() *Memory {
	return &Memory{
		data: make([]Message, 0),
	}
}

func (s *Memory) AddMessage(mgs adding.Message) error {
	s.rwLock.Lock()
	defer s.rwLock.Unlock()

	s.data = append(s.data, Message{
		Timestamp: mgs.Timestamp,
		Message:   mgs.Message,
	})

	sort.Slice(s.data, func(i, j int) bool {
		return s.data[i].Timestamp < s.data[j].Timestamp
	})

	return nil
}

func (s *Memory) Last(limit int) []listing.Message {
	s.rwLock.RLock()
	defer s.rwLock.RUnlock()

	offset := GetLastItemsOffset(len(s.data), limit)
	items := s.data[offset:]

	retVec := make([]listing.Message, len(items), len(items))
	for i, message := range items {
		retVec[i] = listing.Message{
			Timestamp: message.Timestamp,
			Message:   message.Message,
		}
	}

	return retVec
}

func GetLastItemsOffset(sliceCount int, last int) int {
	return int(math.Max(float64(0), float64(sliceCount-last)))
}
