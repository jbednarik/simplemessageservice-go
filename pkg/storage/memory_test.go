package storage

import (
	"fmt"
	"livesport_hw/pkg/adding"
	"livesport_hw/pkg/listing"
	"math"
	"sort"
	"testing"
)

func TestMemory_AddMessage(t *testing.T) {
	store := NewMemory()

	msg1 := adding.Message{
		Timestamp: 123,
		Message:   "Hello",
	}

	msg2 := adding.Message{
		Timestamp: 321,
		Message:   "Hello",
	}

	t.Run("simple add", func(t *testing.T) {
		if err := store.AddMessage(msg2); err != nil {
			t.Fatal(err)
		}

		if store.data[0].Message != msg2.Message {
			t.Errorf("wants: %v, got: %v", msg2, store.data[0])
		}
		if store.data[0].Timestamp != msg2.Timestamp {
			t.Errorf("wants: %v, got: %v", msg2, store.data[0])
		}
	})

	t.Run("add older preserver order", func(t *testing.T) {
		if err := store.AddMessage(msg1); err != nil {
			t.Fatal(err)
		}

		if store.data[0].Message != msg1.Message {
			t.Errorf("wants: %v, got: %v", msg1, store.data[0])
		}
		if store.data[0].Timestamp != msg1.Timestamp {
			t.Errorf("wants: %v, got: %v", msg1, store.data[0])
		}
	})

	isSorted := sort.SliceIsSorted(store.data, func(i, j int) bool {
		return store.data[i].Timestamp < store.data[j].Timestamp
	})

	if !isSorted {
		t.Errorf("order is not preserved: %v", store.data)
	}
}

func TestMemory_Last(t *testing.T) {
	store := NewMemory()

	for i := 0; i < 10; i++ {
		msg := adding.Message{
			Timestamp: int64(i),
			Message:   fmt.Sprintf("Hello %d", i),
		}
		if err := store.AddMessage(msg); err != nil {
			t.Fatal(err)
		}
	}

	assertLastMessages := func(t *testing.T, got []Message, wants []listing.Message) {
		if len(wants) != len(got) {
			t.Errorf("got: %d, wants: %d", len(got), len(wants))
		}

		for i := range got {
			if got[i].Message != wants[i].Message {
				t.Errorf("got: %v, wants: %v", got[i], wants[i])
			}

			if got[i].Timestamp != wants[i].Timestamp {
				t.Errorf("got: %v, wants: %v", got[i], wants[i])
			}
		}
	}

	t.Run("limit 5", func(t *testing.T) {
		limit := 5
		checkLastWithLimit(t, limit, store, assertLastMessages)
	})

	t.Run("limit total", func(t *testing.T) {
		limit := len(store.data)
		checkLastWithLimit(t, limit, store, assertLastMessages)
	})

	t.Run("limit when not enough messages in store", func(t *testing.T) {
		limit := 15
		checkLastWithLimit(t, limit, store, assertLastMessages)
	})
}

func checkLastWithLimit(t *testing.T, limit int, store *Memory, assertLastMessages func(t *testing.T, wants []Message, got []listing.Message)) {
	c := int(math.Min(float64(len(store.data)-(len(store.data)-limit)), float64(len(store.data))))
	wants := make([]Message, 0, c)
	for i := 0; i < cap(wants); i++ {
		msg := Message{
			Timestamp: int64(len(store.data) - cap(wants) + i),
			Message:   fmt.Sprintf("Hello %d", len(store.data)-cap(wants)+i),
		}
		wants = append(wants, msg)
	}

	got := store.Last(limit)
	assertLastMessages(t, wants, got)
}
