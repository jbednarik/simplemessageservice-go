package listing

type Repository interface {
	// Get last limit messages in ASC order
	GetLastMessages(limit int) []Message
}
