package commands

import (
	"fmt"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
	"strings"
)

var (
	cfgFilepath string
)

var rootCmd = &cobra.Command{
	Use:   "cli",
	Short: "A brief description of your application",
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	var err error

	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFilepath, "config", "config.toml", "config file (default is config.toml)")
	err = viper.BindPFlag("config", rootCmd.PersistentFlags().Lookup("config"))
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "error binding flag: %v\n", err)
		os.Exit(1)
	}
}

func initConfig() {
	if cfgFilepath != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFilepath)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		viper.AddConfigPath(".")
		viper.AddConfigPath(home)
		viper.SetConfigName("config")
	}

	viper.SetEnvPrefix("sml")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
	viper.WatchConfig()
}
