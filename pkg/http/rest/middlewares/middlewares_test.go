package middlewares

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestJsonContentType(t *testing.T) {

	handlerToTest := JsonContentType(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// empty
	}))
	req := httptest.NewRequest("GET", "http://testing", nil)

	rr := httptest.NewRecorder()
	handlerToTest.ServeHTTP(rr, req)

	if rr.Header().Get(ContentType) != JsonContentTypeValue {
		t.Errorf("content type not found; want: %s", JsonContentTypeValue)
	}
}
