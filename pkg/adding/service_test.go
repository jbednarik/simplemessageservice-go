package adding

import "testing"

func TestAddMessage(t *testing.T) {
	rpo := &repoMock{addCalled: false}
	svc := NewService(rpo)

	msg := Message{
		Timestamp: 1,
		Message:   "abc",
	}

	svc.AddMessage(msg)

	if !rpo.addCalled {
		t.Errorf("repository was not called")
	}
}

type repoMock struct {
	addCalled bool
}

func (r *repoMock) Add(message Message) error {
	r.addCalled = true
	return nil
}
