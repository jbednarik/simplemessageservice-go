package rest

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"livesport_hw/pkg/adding"
	"livesport_hw/pkg/http/rest/middlewares"
	"livesport_hw/pkg/listing"
	"log"
	"net/http"
)

type MessageServices struct {
	Adding  adding.Service
	Listing listing.Service
}

type PostMessageRequest struct {
	Timestamp *int64  `json:"ts"`
	Message   *string `json:"msg"`
}

type PostMessageResponse struct {
	Status string `json:"status"`
}

type GetMessageResponse struct {
	Timestamp int64  `json:"ts"`
	Message   string `json:"msg"`
}

const (
	PostMessageStatusOk  = "ok"
	PostMessageStatusNok = "nok"
)

type MessageHandlers struct {
	logger   *log.Logger
	services MessageServices
}

func Handler(logger *log.Logger, services MessageServices) http.Handler {

	handler := newMessageHandlers(logger, services)
	router := mux.NewRouter()
	router.Use(middlewares.JsonContentType)
	router.Methods(http.MethodGet).Path("/messages").HandlerFunc(handler.GetMessagesHandler)
	router.Methods(http.MethodPost).Path("/message").HandlerFunc(handler.PostMessageHandler)

	return router
}

func newMessageHandlers(logger *log.Logger, services MessageServices) *MessageHandlers {
	return &MessageHandlers{
		logger:   logger,
		services: services,
	}
}

func (h *MessageHandlers) GetMessagesHandler(w http.ResponseWriter, _ *http.Request) {
	messages := h.services.Listing.GetLastMessages(100)

	vec := make([]GetMessageResponse, 0, len(messages))
	for _, message := range messages {
		vec = append(vec, GetMessageResponse{
			Timestamp: message.Timestamp,
			Message:   message.Message,
		})
	}

	w.WriteHeader(http.StatusOK)
	enc := json.NewEncoder(w)
	err := enc.Encode(&vec)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *MessageHandlers) PostMessageHandler(w http.ResponseWriter, req *http.Request) {
	dec := json.NewDecoder(req.Body)
	dec.DisallowUnknownFields()
	enc := json.NewEncoder(w)

	var requestPayload PostMessageRequest
	err := dec.Decode(&requestPayload)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Printf("Error decoding requst payload: %v", err)
		if err := enc.Encode(PostMessageResponse{Status: PostMessageStatusNok}); err != nil {
			h.logger.Printf("Errof writing response: %v", err)
		}
		return
	}

	if requestPayload.Message == nil {
		w.WriteHeader(http.StatusBadRequest)
		if err := enc.Encode(PostMessageResponse{Status: PostMessageStatusNok}); err != nil {
			h.logger.Printf("Errof writing response: %v", err)
		}
		return
	}

	if requestPayload.Timestamp == nil {
		w.WriteHeader(http.StatusBadRequest)
		if err := enc.Encode(PostMessageResponse{Status: PostMessageStatusNok}); err != nil {
			h.logger.Printf("Errof writing response: %v", err)
		}
		return
	}

	err = h.services.Adding.AddMessage(adding.Message{
		Timestamp: *requestPayload.Timestamp,
		Message:   *requestPayload.Message,
	})
	if err != nil {
		h.logger.Printf("Error decoding requst payload: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	if err := enc.Encode(PostMessageResponse{Status: PostMessageStatusOk}); err != nil {
		h.logger.Printf("Errof writing response: %v", err)
	}
}
