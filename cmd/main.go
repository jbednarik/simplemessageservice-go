package main

import (
	"livesport_hw/cmd/commands"
)

func main() {
	commands.Execute()
}
