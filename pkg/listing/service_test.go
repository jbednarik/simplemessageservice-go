package listing

import "testing"

func TestGetLastMessages(t *testing.T) {
	rpo := &repoMock{limit: nil}
	svc := NewService(rpo)

	want := 2
	svc.GetLastMessages(want)

	got := *rpo.limit

	if got != want {
		t.Errorf("got: %d, want: %d", got, want)
	}
}

type repoMock struct {
	limit *int
}

func (r *repoMock) GetLastMessages(limit int) []Message {
	l := limit
	r.limit = &l
	return nil
}
