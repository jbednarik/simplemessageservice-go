package middlewares

import (
	"net/http"
)

const (
	ContentType          string = "Content-Type"
	JsonContentTypeValue string = "application/json; charset=utf-8"
)

func JsonContentType(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set(ContentType, JsonContentTypeValue)
		next.ServeHTTP(w, r)
	})
}
