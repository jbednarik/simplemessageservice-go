package adding

type Service interface {
	AddMessage(messages ...Message) error
}

type service struct {
	repository Repository
}

func NewService(repository Repository) Service {
	return &service{
		repository: repository,
	}
}

func (s *service) AddMessage(messages ...Message) error {
	for _, message := range messages {
		s.repository.Add(message)
	}
	return nil
}
