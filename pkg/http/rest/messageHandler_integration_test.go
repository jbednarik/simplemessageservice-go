//+build integration

package rest

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"livesport_hw/pkg/adding"
	"livesport_hw/pkg/http/rest/middlewares"
	"livesport_hw/pkg/listing"
	"livesport_hw/pkg/storage"
	"log"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"os"
	"sort"
	"sync"
	"testing"
	"time"
)

// Use DequeMemory for memory limiting purposes
func setup(logger *log.Logger, repository *storage.DequeMemory) *httptest.Server {
	messageServices := MessageServices{
		Adding:  adding.NewService(repository),
		Listing: listing.NewService(repository),
	}
	handler := Handler(logger, messageServices)
	ts := httptest.NewServer(handler)
	return ts
}

func TestHandlerPost(t *testing.T) {
	logger := log.New(os.Stdout, "", log.LstdFlags)
	repository := storage.NewDequeMemory(1000)
	ts := setup(logger, repository)
	defer ts.Close()

	client := ts.Client()
	baseTime := 0
	postMessageRequests := make([]PostMessageRequest, 400, 400)

	for i := 0; i < len(postMessageRequests); i++ {
		ts := int64(baseTime + i)
		msg := "321"
		payload := PostMessageRequest{
			Timestamp: &ts,
			Message:   &msg,
		}
		postMessageRequests[i] = payload
	}

	for _, payload := range postMessageRequests {
		reqBodyBytes, err := json.Marshal(payload)
		if err != nil {
			t.Fatal(err)
		}
		req, err := http.NewRequest(http.MethodPost, ts.URL+"/message", bytes.NewBuffer(reqBodyBytes))
		if err != nil {
			t.Fatal(err)
		}
		resp, err := client.Do(req)
		if err != nil {
			t.Fatal(err)
		}

		assertCode(t, resp.StatusCode, http.StatusOK)
		assertContentType(t, resp.Header, middlewares.JsonContentTypeValue)

		var postResponseModel PostMessageResponse
		dec := json.NewDecoder(resp.Body)
		err = dec.Decode(&postResponseModel)
		if err != nil {
			t.Fatal(err)
		}
		if postResponseModel.Status != PostMessageStatusOk {
			t.Errorf("got: %v, expected: %v", postResponseModel.Status, PostMessageStatusOk)
		}
	}

	want := postMessageRequests[len(postMessageRequests)-100:]
	got := getLastMessages(t, client, ts.URL)

	if len(got) != len(want) {
		t.Errorf("got: %v, want: %v", len(got), len(want))
	}

	for i, g := range got {
		if g.Timestamp != *want[i].Timestamp {
			t.Errorf("got: %v, want: %v", g.Timestamp, *want[i].Timestamp)
		}

		if g.Message != *want[i].Message {
			t.Errorf("got: %v, want: %v", g.Message, *want[i].Message)
		}
	}
}

func TestHandlerPostParallel(t *testing.T) {
	logger := log.New(os.Stdout, "", log.LstdFlags)
	repository := storage.NewDequeMemory(1000)
	ts := setup(logger, repository)
	defer ts.Close()

	const (
		clients = 4
	)

	ctx, cancel := context.WithCancel(context.Background())
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()

		for i := 0; i < clients; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()
				postMessageGenerator := randomMessagePostGenerator(ctx, 1)

				client := ts.Client()

				for request := range postMessageGenerator {
					reqBodyBytes, err := json.Marshal(request)
					if err != nil {
						t.Fatal(err)
					}
					req, err := http.NewRequest(http.MethodPost, ts.URL+"/message", bytes.NewBuffer(reqBodyBytes))
					if err != nil {
						t.Fatal(err)
					}
					resp, err := client.Do(req)
					if err != nil {
						t.Fatal(err)
					}

					assertCode(t, resp.StatusCode, http.StatusOK)
					assertContentType(t, resp.Header, middlewares.JsonContentTypeValue)

					var postResponseModel PostMessageResponse
					dec := json.NewDecoder(resp.Body)
					err = dec.Decode(&postResponseModel)
					if err != nil {
						t.Fatal(err)
					}
					if postResponseModel.Status != PostMessageStatusOk {
						t.Errorf("got: %v, expected: %v", postResponseModel.Status, PostMessageStatusOk)
					}
				}
			}()
		}
	}()

	// sleep between 300 - 1000ms to generate some messages
	time.Sleep(time.Millisecond * time.Duration(rand.Intn(1000-300)+300))

	//get snapshot of last 100 messages and check if they are in chronological order
	got := getLastMessages(t, ts.Client(), ts.URL)
	if !checkChronologicalOrder(got) {
		t.Errorf("received slices are not in chronological order")
	}

	// signal all posting clients to stop
	cancel()
	// wait until all clients finish
	wg.Wait()
}

func checkChronologicalOrder(messages []GetMessageResponse) bool {
	return sort.SliceIsSorted(messages,
		func(i, j int) bool {
			return messages[i].Timestamp < messages[j].Timestamp
		})
}

func getLastMessages(t *testing.T, client *http.Client, baseUrl string) []GetMessageResponse {
	req, err := http.NewRequest(http.MethodGet, baseUrl+"/messages", nil)
	if err != nil {
		t.Fatal(err)
	}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("got: %d, expected: %d", resp.StatusCode, http.StatusOK)
	}

	var got []GetMessageResponse
	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(&got)
	if err != nil {
		t.Fatal(err)
	}
	return got
}

func randomMessagePostGenerator(ctx context.Context, backPressure int) <-chan PostMessageRequest {
	out := make(chan PostMessageRequest, backPressure)
	go func() {
		defer close(out)
		rng := rand.New(rand.NewSource(time.Now().UnixNano()))
	loop:
		for {
			select {
			case <-ctx.Done():
				break loop
			default:
				min := 10
				max := 100000
				ts := time.Now().UTC().Unix() + int64(rng.Intn(max-min)+min)
				msg := fmt.Sprintf("Message sent at %d", ts)
				out <- PostMessageRequest{
					Timestamp: &ts,
					Message:   &msg,
				}
			}
		}
	}()

	return out
}

func assertContentType(t *testing.T, header http.Header, want string) {
	got := header.Get("Content-type")
	if got != want {
		t.Errorf("got: %v, want: %v", got, want)
	}
}
