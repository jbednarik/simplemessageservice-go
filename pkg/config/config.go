package config

import (
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
	"net"
)

type Config struct {
	Config string `json:"config"`
	Server Server `json:"server"`
}

type Server struct {
	Port    uint16 `json:"port"`
	Address net.IP `json:"address"`
	TlsKey  string `json:"tlsKey"`
	TlsCert string `json:"tlsCert"`
}

func LoadConfig(v *viper.Viper) (*Config, error) {
	var cfg Config
	err := v.Unmarshal(&cfg, func(dc *mapstructure.DecoderConfig) {
		dc.DecodeHook = mapstructure.StringToIPHookFunc()
	})
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}
