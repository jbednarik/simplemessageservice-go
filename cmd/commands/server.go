package commands

import (
	"context"
	"crypto/tls"
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"livesport_hw/pkg/adding"
	"livesport_hw/pkg/config"
	"livesport_hw/pkg/http/rest"
	"livesport_hw/pkg/listing"
	"livesport_hw/pkg/storage"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

const (
	Reload = iota
	Shutdown
)

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "The 'server' command runs the simple message logging server.",
	Run: func(cmd *cobra.Command, args []string) {

		cancel, eventSignals := listenForCtrlC()
		defer cancel()

		cfg, err := config.LoadConfig(viper.GetViper())
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "error loading configuration file: %v\n", err)
			os.Exit(1)
		}

		tlsConfig, err := loadTls(cfg)
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "error loading certifiates: %v\n", err)
			os.Exit(1)
		}

		logger := log.New(os.Stdout, "", log.LstdFlags)
		repo := storage.NewDequeMemory(1000)
		services := rest.MessageServices{
			Adding:  adding.NewService(repo),
			Listing: listing.NewService(repo),
		}
		handler := rest.Handler(logger, services)

		wg := sync.WaitGroup{}
		wg.Add(1)
		go func() {
			defer wg.Done()
			serverDownChan := make(chan struct{}, 1)
			defer close(serverDownChan)
			for {
				logger.Printf("Starting server on %v:%v\n", cfg.Server.Address, cfg.Server.Port)

				var svr http.Server

				svr = http.Server{
					Addr:              fmt.Sprintf("%v:%v", cfg.Server.Address, cfg.Server.Port),
					Handler:           handler,
					TLSConfig:         tlsConfig,
					ReadTimeout:       time.Second * 10,
					ReadHeaderTimeout: time.Second * 10,
					WriteTimeout:      time.Second * 10,
					IdleTimeout:       time.Second * 10,
					MaxHeaderBytes:    0,
					TLSNextProto:      nil,
					ConnState:         nil,
					ErrorLog:          nil,
					BaseContext:       nil,
					ConnContext:       nil,
				}

				go func() {
					var err error

					if svr.TLSConfig != nil {
						err = svr.ListenAndServeTLS(cfg.Server.TlsCert, cfg.Server.TlsKey)
					} else {
						err = svr.ListenAndServe()
					}

					if err != http.ErrServerClosed {
						_, _ = fmt.Fprintf(os.Stderr, "error starting server: %v\n", err)
						os.Exit(1)
					}
					serverDownChan <- struct{}{}
				}()

				select {
				case msg := <-eventSignals:

					switch msg {
					case Reload:
						var err error
						logger.Printf("Reloading due to configuration change\n")
						if err := svr.Shutdown(context.Background()); err != nil {
							_, _ = fmt.Fprintf(os.Stderr, "error shuting down server: %v\n", err)
							os.Exit(1)
						}

						<-serverDownChan

						var newConfig *config.Config
						if newConfig, err = config.LoadConfig(viper.GetViper()); err != nil {
							_, _ = fmt.Fprintf(os.Stderr, "Error loading configuration file:\n%v\n", err)
						} else {
							cfg = newConfig
						}

						var newTlsConfig *tls.Config
						newTlsConfig, err = loadTls(cfg)
						if err != nil {
							_, _ = fmt.Fprintf(os.Stderr, "error loading certifiates: %v\n", err)
						} else {
							tlsConfig = newTlsConfig
						}
					case Shutdown:
						// Hack to get rid of ^C from the terminal if possible
						fmt.Print("\r")
						logger.Printf("CTRL+C detected...\n")
						logger.Printf("Shutting down the server...\n")
						if err := svr.Shutdown(context.Background()); err != nil {
							panic(err)
						}
						<-serverDownChan
						return
					}
				}
			}

		}()

		viper.OnConfigChange(func(in fsnotify.Event) {
			logger.Printf("file configuration change detected - reloading server...\n")
			eventSignals <- Reload
		})

		wg.Wait()
	},
}

func listenForCtrlC() (context.CancelFunc, chan int) {
	_, cancel := context.WithCancel(context.Background())

	eventSignals := make(chan int, 1)

	go func() {
		interruptChan := make(chan os.Signal, 2)
		signal.Notify(interruptChan, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
		<-interruptChan
		cancel()
		eventSignals <- Shutdown
	}()

	return cancel, eventSignals
}

func init() {
	rootCmd.AddCommand(serverCmd)

	serverCmd.PersistentFlags().IPP("address", "l", net.ParseIP("0.0.0.0"), "The ip address server will listen on.")
	err := viper.BindPFlag("server.address", serverCmd.PersistentFlags().Lookup("address"))
	if err != nil {
		panic(err)
	}

	serverCmd.Flags().Uint16P("port", "p", 5000, "The port number server will listen on.")
	err = viper.BindPFlag("server.port", serverCmd.Flags().Lookup("port"))
	if err != nil {
		panic(err)
	}

	serverCmd.PersistentFlags().StringP("tls-cert", "", "", "TLS certificate.")
	err = viper.BindPFlag("server.tlsCert", serverCmd.PersistentFlags().Lookup("tls-cert"))
	if err != nil {
		panic(err)
	}

	serverCmd.PersistentFlags().StringP("tls-key", "", "", "TLS key.")
	err = viper.BindPFlag("server.tlsKey", serverCmd.PersistentFlags().Lookup("tls-key"))
	if err != nil {
		panic(err)
	}
}

func loadTls(c *config.Config) (*tls.Config, error) {

	if c.Server.TlsCert != "" && c.Server.TlsKey == "" || c.Server.TlsCert == "" && c.Server.TlsKey != "" {
		return nil, fmt.Errorf("tlscert and tlskey must be specified")
	}

	if c.Server.TlsKey == "" && c.Server.TlsCert == "" {
		return nil, nil
	}

	cer, err := tls.LoadX509KeyPair(c.Server.TlsCert, c.Server.TlsKey)
	if err != nil {
		return nil, err
	}

	cfg := tls.Config{Certificates: []tls.Certificate{cer}}
	return &cfg, nil
}
