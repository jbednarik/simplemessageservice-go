FROM golang:1.13-alpine as builder

WORKDIR /build

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY ./cmd ./cmd
COPY ./pkg ./pkg

RUN CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o sml ./cmd/main.go

RUN mkdir /dist
RUN mv /build/sml /dist/sml
RUN mkdir /data



FROM scratch

COPY --chown=0:0 --from=builder /dist/sml /app/sml

COPY --chown=65534:0 --from=builder /data /app/data
USER 65534
WORKDIR /app/data

USER 65534

VOLUME ["/app/data"]
EXPOSE 5000

CMD ["server"]

ENTRYPOINT ["/app/sml"]